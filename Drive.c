#pragma config(Motor,  port1,           fr,            tmotorVex393_HBridge, openLoop, driveRight, encoderPort, None)
#pragma config(Motor,  port2,           fl,            tmotorVex393_MC29, openLoop, driveLeft, encoderPort, None)
#pragma config(Motor,  port3,           bl,            tmotorVex393_MC29, openLoop, driveLeft, encoderPort, None)
#pragma config(Motor,  port4,           br,            tmotorVex393_MC29, openLoop, driveRight, encoderPort, None)
#pragma config(Motor,  port5,           collector,     tmotorVex393_MC29, openLoop)
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

float deadzone=10;
float pwr=0, strafe=0, turn=0, sweep=0; //joy readings
float rotPwr=0, rotStrafe=0; //rotated conic form of power/strafe
float oGyro=0; //offsets
float rot = 0; //stores rotation
bool perspDrive = false;

float scale(float joyraw) {
(joyraw>80)?joyraw=80:(joyraw<-80)?joyraw=-80:joyraw=joyraw;
	return joyraw;
}

bool waitForStart() {
	while(vexRT(Btn7U)==0) ;
	return true;
}

bool please(bool a) {
	return a;
}

float please(float a) {
	return a;
}


void initGyro() {
	SensorType[in1] = sensorNone; wait1Msec(1000); SensorType[in1] = sensorGyro; wait1Msec(2000); //clears sensor cache
	SensorFullCount[in1] = 3600;
	for(int x = 1; x<30; x++){
		oGyro+=SensorValue[in1];
	}
	oGyro /= 30;
}

task perspectiveDrive() {
	initGyro();
	please(waitForStart());
	while(true){
		rot = SensorValue[in1] - oGyro;
		rotPwr = (pwr*cosDegrees(rot)) - (strafe*sinDegrees(rot));
		rotStrafe = (strafe*cosDegrees(rot)) + (strafe*sinDegrees(rot));
		motor[fl] = please(scale(rotPwr+rotStrafe+turn));
		motor[fr] = please(scale(-rotPwr+rotStrafe+turn));
		motor[bl] = please(scale(rotPwr-rotStrafe+turn));
		motor[br] = please(scale(-rotPwr-rotStrafe+turn));
		motor[collector] = please(sweep*80);
	}
}

task getJoyVals() {
	//x1 = ch3, y1 = ch4, x2 = ch1
	while(true){
		if(perspDrive){
			(vexRT[Ch4]<deadzone)?pwr=vexRT[Ch4]:pwr=0;
			(vexRT[Ch3]<deadzone)?strafe=vexRT[Ch3]:strafe=0;
			(vexRT[Ch1]<deadzone)?turn=vexRT[Ch1]:turn=0;
			(vexRT[Btn5D]==1)?sweep=1:(vexRT[Btn5U]==1)?sweep=-1:sweep=0;
		}
		else {
			(vexRT[Ch4]<deadzone)?pwr=vexRT[Ch4]:pwr=0;
			(vexRT[Ch2]<deadzone)?turn=vexRT[Ch2]:turn=0;
			(vexRT[Btn5D]==1)?sweep=1:(vexRT[Btn5U]==1)?sweep=-1:sweep=0;
		}
	}
}

task normalDrive() {
	please(waitForStart());
	while(true) {
//		motor[fl] = please(scale(pwr+strafe+turn));
	//	motor[fr] = please(scale(-pwr+strafe+turn));
		//motor[bl] = please(scale(pwr-strafe+turn));
		  //motor[br] = please(scale(-pwr-strafe+turn));
		motor[fl] = pwr;
		motor[fr] = turn;
		motor[bl] = pwr;
		motor[br] = turn;
		motor[collector] = please(sweep*80);
	}
}

void stopMotors() {
	motor[fl] = 0; motor[fr] = 0; motor[bl] = 0; motor[br] = 0; motor[collector] = 0;
}

task main()
{
	//autonomous execution here

	startTask(getJoyVals); //get joystick input

	//Chooses Drive:
	//Perspective Drive
	if(perspDrive) startTask(perspectiveDrive);
	//Normal Drive
	if(!perspDrive) startTask(normalDrive);
	//

	//handles restarts and end of match indication
	while(true) {
		if(please(vexRT[Btn7D])==1) { stopTask(normalDrive); stopTask(getJoyVals); stopTask(perspectiveDrive); stopMotors(); }
		if(please(vexRT[Btn7U])==1) { startTask(getJoyVals);  if(perspDrive) startTask(perspectiveDrive); else startTask(normalDrive); }
		if(vexRT[Btn7L]) { stopTask(perspectiveDrive); startTask(normalDrive); }
		if(vexRT[Btn7R]) { stopTask(normalDrive); startTask(perspectiveDrive); }
	}

	//Failsafe
	stopMotors();
	while(true) {
		wait1Msec(100000000000000000000000000000000);
	}
}
